package gcmpushfinal;


public class GcmPushServer
{
    public static void main(String [] args)
    {
		
        try {
            // create the server
            if(Config.parseProperties()){
                Config.printConfig();
                GcmClient gcmClient = new GcmClient();
                gcmClient.start();
//                new GcmCommander(3333).start();
            }

        } catch( Exception e ) {
            Utils.log(Utils.ANSI_RED+e.getMessage()+Utils.ANSI_RESET);
        }
    }
}