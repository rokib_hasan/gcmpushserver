/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gcmpushfinal;

import static gcmpushfinal.Config.UDP_HEADER;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rokib
 */
public class GcmCommander {
    DatagramSocket serverSocket;
    byte[] receiveData = new byte[1024];
    byte[] sendData = new byte[1024];
    
    int bindPort = 3333;

    public GcmCommander(int bindPort) {
        try {
            serverSocket = new DatagramSocket(bindPort);
        } catch (SocketException ex) {
           ex.printStackTrace();
        }
    }
    
    public void start()
    {
       new Thread(new Runnable() {

           @Override
           public void run() {
               while(true)
               {
                  try
                  {
                        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                        serverSocket.receive(receivePacket);
                        String command = new String( receivePacket.getData(),0, receivePacket.getLength());
                        Utils.log(Utils.ANSI_PURPLE+"COMMAND RECEIVED: " + command+Utils.ANSI_RESET);
                        if(parseCommand(command)){
                            InetAddress IPAddress = receivePacket.getAddress();
                            int port = receivePacket.getPort();
                            sendData = "OK".getBytes();
                            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                            serverSocket.send(sendPacket);
                        }
                       
                  }
                  catch(Exception ex){
                      ex.printStackTrace();
                  }
                  
               }
           }
       }).start();
      
    }
    
    private boolean parseCommand(String command){
       try{
           if(Config.updateProperties(command)){
               return true;
           }
       }
       catch(Exception ex){
           ex.printStackTrace();
       }
       return false;
    }
    
    
    
    
}
