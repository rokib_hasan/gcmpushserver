/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gcmpushfinal;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Config
{
  public static int THREAD_POOL_SIZE = 1000;
  
  public static String GCM_SENDER_ID;
  
  public static String GCM_API_KEY;
  public static String HTTP_REQ_HEADER;
  public static String HTTP_RES_HEADER;
  public static String SWITCH_COUNT;
  public static String UDP_HEADER;
  public static String SNI_WIFI;
  public static String[] AUTH_URL;
  public static String PROTO_VERSION;
  public static String PROXY_PORT_RANGE;
  
  public Config() {}
  
  public static Properties prop = new Properties();
  
  public static boolean parse(String file) {
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(file));
      
      THREAD_POOL_SIZE = Integer.parseInt(br.readLine());
      GCM_SENDER_ID = br.readLine();
      GCM_API_KEY = br.readLine();
      AUTH_URL = br.readLine().split(";");
      UDP_HEADER = br.readLine();
      HTTP_REQ_HEADER = br.readLine();
      HTTP_RES_HEADER = br.readLine();
      SWITCH_COUNT = br.readLine();
      SNI_WIFI = br.readLine();
      
      return true;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return false;
    }
    finally {
      try {
        br.close();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }
  

  public static boolean parseProperties()
  {
    InputStream input = null;
    try
    {
      input = new FileInputStream("config.properties");
      prop.load(input);
      
      THREAD_POOL_SIZE = Integer.parseInt(prop.getProperty("threadpoolsize"));
      GCM_SENDER_ID = prop.getProperty("senderid");
      GCM_API_KEY = prop.getProperty("severkey");
      AUTH_URL = prop.getProperty("authurl").split(";");
      UDP_HEADER = prop.getProperty("udpheader");
      HTTP_REQ_HEADER = prop.getProperty("httpreqheader");
      HTTP_RES_HEADER = prop.getProperty("httpresheader");
      SWITCH_COUNT = prop.getProperty("packetcount");
      SNI_WIFI = prop.getProperty("sni");
      PROTO_VERSION = prop.getProperty("protoversion");
      PROXY_PORT_RANGE = prop.getProperty("proxyrange");

      return true;
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
      return false;
    } finally {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
  

  public static boolean updateProperties(String keyPair)
  {
    try
    {
      String[] temp = keyPair.split("=");
      if (temp.length != 2) {
        return false;
      }
      String key = temp[0];
      String value = temp[1];
      
      if (prop.getProperty(key) == null) {
        return false;
      }
      FileOutputStream output = new FileOutputStream("config.properties");
      prop.setProperty(key, value);
      prop.store(output, null);
      
      output.close();
    }
    catch (Exception io) {
      io.printStackTrace();
    }
    return true;
  }
  
  public static void printConfig() {
    
    for (String key : prop.stringPropertyNames()) {
      String value = prop.getProperty(key);
      Utils.log(Utils.ANSI_GREEN + key + ": " + value + Utils.ANSI_RESET );
    }
  }
}
