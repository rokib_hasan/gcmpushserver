/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gcmpushfinal;

import static gcmpushfinal.Config.prop;
import java.util.Map;

/**
 *
 * @author rokib
 */
public class GcmClient extends CCSClient {

    
    
    public void start(){
         executor.submit(new CCSClient.ConnectionCreator());
    }
    
    @Override
    protected void handleUpstreamMessage(Map<String, Object> jsonObject) {
       String category = (String) jsonObject.get("category");
        String from = (String) jsonObject.get("from");
        @SuppressWarnings("unchecked")
        Map<String, String> payload = (Map<String, String>) jsonObject.get("data");
        if (! payload.containsKey( "AuthData" ) )
        {
               Utils.log("Authdata missing...");
               return;
        }
        int versionCode;
        int opcode;
        versionCode = (payload.get("VersionCode") == null)? 0:Integer.parseInt(payload.get("VersionCode"));
        opcode = (payload.get("Opcode") == null)? 0:Integer.parseInt(payload.get("Opcode"));
        
        Utils.log(Utils.ANSI_BLUE+"Received: Auth data VersionCode: "+versionCode+" Opcode: "+opcode+Utils.ANSI_RESET);
        if(!isVersionCodeValid(versionCode,from,payload)){
            return;
        }
        String authData = payload.get("AuthData");
        Runnable runnable = new WorkThread(authData, Config.AUTH_URL[random.nextInt(128)%Config.AUTH_URL.length],from, versionCode,opcode);
        executor.submit(runnable);
    }
    
    private boolean isVersionCodeValid(int versionCode, String from,Map<String, String> payload){
        
        int restrictedVersionCode = Integer.parseInt(prop.getProperty("VersionCode"));
        if(versionCode< restrictedVersionCode){
             Utils.log(Utils.ANSI_RED+"Invalid version code "+versionCode+Utils.ANSI_RESET);
             payload.clear();
             payload.put("AuthResp", "2338C6C180CB22CD97445FFB9BA2ADAA");
             String authResponse = Utils.createJsonMessage(from, nextMessageId(), payload,
                                null, Utils.TIME_TO_LIVE, false);

                try {
                        sendDownstreamMessage(authResponse);
                } catch (Exception e) {
//                            logArea.logMessage( "Not connected anymore, echo message is not sent: " + e.getMessage() );
                    Utils.log(Utils.ANSI_RED+"Not connected anymore, downstream msg is not sent: " + e.getMessage()+Utils.ANSI_RESET);
                }
            return false;
        }
        return true;
    }
    
}
