package com.voillo.authtester;


import gcmpushfinal.Utils;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;



public class AuthApi {
	
//	private String AUTH_SERVER;
        private static final String AUTH_KEY = "A9EFX1MIE9XQOWQZ";

        
	public static int AUTH_URL_PORT[]={8091, 7582, 6375, 5947, 4720, 9513};
	
	public static int index = 0;
	
	
	public static String sendGetAuthInfoMsg(String authIp, byte[] msg){
		InetAddress address=null;
		try {
			address = InetAddress.getByName(authIp);
		} catch (UnknownHostException e) {
			 Utils.log(Utils.ANSI_RED+ e.getMessage()+Utils.ANSI_RESET);
                        return null;
		}
                DatagramSocket dsocket = null;
                try {
			dsocket = new DatagramSocket();
			dsocket.setSoTimeout(3000);
		} catch (Exception e) {
			 Utils.log(Utils.ANSI_RED+ e.getMessage()+Utils.ANSI_RESET);
                        return null;
		}
		
                byte[] recData = new byte[512];
                DatagramPacket rcvPkt=new DatagramPacket(recData, recData.length);
                try
                {
//                    DatagramPacket sndPkt = new DatagramPacket(msg, msg.length,address, AUTH_URL_PORT[new Random().nextInt(32) % AUTH_URL_PORT.length]);
                    DatagramPacket sndPkt = new DatagramPacket(msg, msg.length,address, AUTH_URL_PORT[(index)%6]);
                
                    dsocket.send(sndPkt);
                    dsocket.receive(rcvPkt);
                    dsocket.close();
                    
                    index++;
                    if(index >= 6){
                        index = 0;
                    }
				
                }
                catch(Exception e){
                        Utils.log(Utils.ANSI_RED+ e.getMessage()+Utils.ANSI_RESET);
                        return null;
                }
//                byte[] data = new byte[rcvPkt.getLength()];
//                System.arraycopy(rcvPkt.getData(), rcvPkt.getOffset(), data, 0, rcvPkt.getLength());
		return Utils.toHexString(rcvPkt.getData(), rcvPkt.getLength());
	}
	
}
